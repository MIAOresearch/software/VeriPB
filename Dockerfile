# syntax=docker/dockerfile:1

FROM ubuntu:24.04

# Build Requirenments
RUN apt-get update && apt-get install --no-install-recommends -y \
		python3 \
		pipx \
		python3-dev \
		g++ \
		libgmp-dev \
		zlib1g-dev \
		python3-pybind11

# Test Requirenments
RUN apt-get install -y make python3-pytest

# Copy From Working Directory
COPY . /app
WORKDIR /app

# Compile VeriPB
RUN pipx install .

# Run VeriPB tests
RUN make test -j

# Create entrypoint for interactive use
ENTRYPOINT [ "bash" ]