#!/bin/bash

# this script requires google perf tools to be installed
# (https://github.com/gperftools/gperftools) or according package from
# package manager

## How to use?
## ```
## ./profiled.sh veripb <options>
## ```

VERIPB_DIR=`dirname $(realpath ${BASH_SOURCE[0]})`

## The path to the libprofiler.so and libtcmalloc.so library potentially need to be adjusted for your system. Choose what to profile by commenting out the right line.
## heap and performance
LD_PRELOAD=/usr/lib64/libprofiler.so:/usr/lib64/libtcmalloc.so CPUPROFILE=prof.out CPUPROFILE_FREQUENCY=1000 HEAPPROFILE=./heap.prof "$@"
## performance only
# LD_PRELOAD=/usr/lib64/libprofiler.so CPUPROFILE=prof.out CPUPROFILE_FREQUENCY=1000 "$@"
## heap only
# LD_PRELOAD=/usr/lib64/libtcmalloc.so HEAPPROFILE=./heap.prof "$@"

py_suffix=`python3-config --extension-suffix`
## process profiles
pprof --addresses --callgrind $VERIPB_DIR/veripb/optimized/pybindings${py_suffix} prof.out > callgrind.out.cpp.profile
## show heap profile
pprof --gv $VERIPB_DIR/veripb/optimized/pybindings${py_suffix} heap.prof.0001.heap

## alternatively for perf:
# perf record --call-graph fp "$@"
# perf report
