#include <cctype>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>
#include <map>
#include <unordered_set>
#include "gzstream.hpp"

#include "BigInt.hpp"
#include "constraints.hpp"

#ifdef PY_BINDINGS
#include <pybind11/functional.h>
#include <pybind11/iostream.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
namespace py = pybind11;
#endif

using std::string_view;

struct FileInfo
{
  FileInfo(std::string _fileName) : fileName(_fileName) {}

  std::string fileName;
  size_t line = 0;
};

class WordIter;

class ParseError : std::runtime_error
{
public:
  std::string fileName = "";
  size_t line = 0;
  size_t column = 0;

  explicit ParseError(const WordIter &it, const std::string &what_arg);

  mutable std::string msg;
  virtual char const *what() const noexcept override
  {
    std::stringstream what;
    if (fileName == "")
    {
      what << "?";
    }
    else
    {
      what << fileName;
    }
    what << ":";
    if (line == 0)
    {
      what << "?";
    }
    else
    {
      what << line;
    }
    what << ":";
    if (column == 0)
    {
      what << "?";
    }
    else
    {
      what << column;
    }
    what << ": " << runtime_error::what();
    msg = what.str();
    return msg.c_str();
  }
};

class WordIter
{
public:
  const char *wordSeperator = " \t";

  FileInfo fileInfo;
  std::string line;
  string_view word;
  size_t start = 0;
  size_t endPos = 0;
  size_t nextStart = 0;
  bool isEndIt = false;

  void next()
  {
    start = nextStart;
    if (start != std::string::npos)
    {
      endPos = line.find_first_of(wordSeperator, start);
      if (endPos == std::string::npos)
      {
        nextStart = endPos;
        endPos = line.size();
      }
      else
      {
        nextStart = line.find_first_not_of(wordSeperator, endPos);
      }

      word = string_view(&line.data()[start], endPos - start);
    }
    else
    {
      word = "";
    }
  }

  /// @brief Go back one word in the line.
  void undo()
  {
    nextStart = start;
    // If we are still at the beginning, then just resetting `nextStart` to start is sufficient. The rest of the function is for the general case when we could do multiple undo.
    if (start == 0)
    {
      return;
    }
    start--;

    if (endPos != std::string::npos)
    {
      // Go back over the whitespace.
      while (start > 0)
      {
        if (line[start] != ' ' && line[start] != '\t')
        {
          break;
        }
        start--;
      }
    }
    endPos = start + 1;

    // Go back over the characters of the previous word.
    while (start > 0)
    {
      if (line[start] == ' ' || line[start] == '\t')
      {
        break;
      }
      start--;
    }

    if (start != 0)
    {
      start += 1;
    }

    word = string_view(&line.data()[start], endPos - start);
  }

private:
  WordIter() : fileInfo("WordIter::EndItterator") { isEndIt = true; }

public:
  static WordIter end;

  static std::istream &getline(std::istream &stream, WordIter &it)
  {
    it.fileInfo.line += 1;
    std::istream &result = std::getline(stream, it.line);
    if (!result.eof() && result.fail())
    {
      throw ParseError(it, "Failed to read line (IOError).");
    }
    if (!it.line.empty() && it.line.back() == '\r')
    {
      // remove trailing \r to support windows files opened under linux
      it.line.pop_back();
    }
    it.init();
    return result;
  }

  WordIter(std::string fileName) : fileInfo(fileName) {}

  WordIter(FileInfo _info) : fileInfo(_info) {}

  void init()
  {
    endPos = 0;
    nextStart = line.find_first_not_of(wordSeperator, endPos);
    next();
  }

  void expect(std::string word)
  {
    if (*this == WordIter::end)
    {
      std::stringstream s;
      s << "Expected '" << word << "'.";
      throw ParseError(*this, s.str());
    }
    else if (this->get() != word)
    {
      std::stringstream s;
      s << "Expected '" << word << "', but found '" << this->get() << "'.";
      throw ParseError(*this, s.str());
    }
  }

  std::stringstream getExpectedSS(std::vector<std::string> &words)
  {
    std::stringstream s;
    s << "Expected one of ";
    bool first = true;
    for (const std::string &word : words)
    {
      if (first)
      {
        first = false;
      }
      else
      {
        s << ", ";
      }
      s << "'" << word << "'";
    }
    return s;
  }

  void expectOneOf(std::vector<std::string> &words)
  {
    if (*this == WordIter::end)
    {
      std::stringstream s = getExpectedSS(words);
      s << "'.";
      throw ParseError(*this, s.str());
    }
    bool found = false;
    for (const std::string &word : words)
    {
      if (this->get() == word)
      {
        found = true;
        break;
      }
    }
    if (!found)
    {
      std::stringstream s = getExpectedSS(words);
      s << ", but found '" << this->get() << "'.";
      throw ParseError(*this, s.str());
    }
  }

  const string_view &get() { return word; }
  const string_view &operator*() const { return word; };
  const string_view *operator->() const { return &word; };

  WordIter &operator++()
  {
    next();
    return *this;
  }

  bool isEnd() const { return start == string_view::npos; }

  friend bool operator==(const WordIter &a, const WordIter &b)
  {
    if (a.isEndIt)
    {
      return (b.start == string_view::npos);
    }
    else if (b.isEndIt)
    {
      return (a.start == string_view::npos);
    }
    else
    {
      return (a.line == b.line) and (a.start == b.start) and
             (a.endPos == b.endPos);
    }
  }
  friend bool operator!=(const WordIter &a, const WordIter &b)
  {
    return !(a == b);
  }

  size_t getColumn() const
  {
    size_t column;
    if (this->start == std::string::npos)
    {
      column = this->line.size() + 1;
    }
    else
    {
      column = this->start + 1;
    }
    return column;
  }

  size_t getLine() const { return this->fileInfo.line; }

  std::string getLineText() const { return this->line; }

  void setLineText(std::string _line)
  {
    line = _line;
    init();
  }

  std::string getFileName() const { return this->fileInfo.fileName; }
};

bool nextLine(igzstream *stream, WordIter *it)
{
  return !!WordIter::getline(*stream, *it);
}

WordIter WordIter::end;

ParseError::ParseError(const WordIter &it, const std::string &what_arg)
    : std::runtime_error(what_arg)
{
  fileName = it.getFileName();
  line = it.getLine();
  column = it.getColumn();
}

/**
 * throws std::invalid_argument or std::out_of_range exception on
 * invalid input
 */
template <typename T>
T parseCoeff(const WordIter &it)
{
  // std::cout  << it->size() << std::endl;
  return parseCoeff<T>(it, 0, it->size());
};

template <typename T>
T parseCoeff(const WordIter &it, size_t start, size_t length);

int parseInt(const WordIter &word, std::string msg, size_t start,
             size_t length)
{
  assert(word->size() >= start + length);
  assert(length > 0);

  if (word.isEnd())
  {
    throw ParseError(word, "Expected Number.");
  }

  const char *it = word->data() + start;
  const char *end = word->data() + start + length;

  bool negated = false;
  if (*it == '+')
  {
    it += 1;
  }
  else if (*it == '-')
  {
    negated = true;
    it += 1;
  }

  if (it == end)
  {
    throw ParseError(word, "Expected Number, only got sign.");
  }
  else if (end - it > 9)
  {
    throw ParseError(word, "Number too large.");
  }

  int res = 0;
  for (; it != end; ++it)
  {
    uint8_t chr = *it - '0';
    res *= 10;
    res += chr;
    if (chr > 9)
    {
      throw ParseError(word, "Expected number.");
    }
  }
  if (negated)
  {
    res = -res;
  }

  return res;
  // try {
  //     return std::stoi(it->substr(start, length));
  // } catch (const std::invalid_argument& e) {
  //     throw ParseError(it, "Expected coefficient, did not get an integer.");
  // } catch (const std::out_of_range& e) {
  //     throw ParseError(it, "Coefficient too large.");
  // }
};

int parseInt(const WordIter &it, std::string msg)
{
  return parseInt(it, msg, 0, it->size());
};

template <>
int parseCoeff<int>(const WordIter &it, size_t start, size_t length)
{
  return parseInt(it, "Expected coefficient", start, length);
}

template <>
BigInt parseCoeff<BigInt>(const WordIter &word, size_t start, size_t length)
{
  assert(word->size() >= start + length);
  assert(length > 0);

  if (word.isEnd())
  {
    throw ParseError(word, "Expected Number.");
  }

  const char *it = word->data() + start;
  if (*it == '+')
  {
    it += 1;
    length -= 1;
  }
  // copy to string to get \0 terminated string
  std::string subString(it, length);
  try
  {
    return BigInt(subString);
  }
  catch (...)
  {
    throw ParseError(word, "Error while parsing number.");
  }
}

Lit parseLit(const WordIter &it, VariableNameManager &mngr,
             const std::string_view *value = nullptr)
{
  if (it == WordIter::end)
  {
    throw ParseError(it, "Expected literal.");
  }

  try
  {
    if (value)
    {
      return mngr.getLit(*value);
    }
    else
    {
      return mngr.getLit(*it);
    }
  }
  catch (const std::invalid_argument &e)
  {
    throw ParseError(it, e.what());
  }
  catch (const std::out_of_range &e)
  {
    throw ParseError(it, e.what());
  }
}

class VarDouplicateDetection
{
  std::vector<bool> contained;
  std::vector<Var> used;

public:
  bool add(Var var)
  {
    used.push_back(var);
    size_t idx = static_cast<size_t>(var);
    if (idx >= contained.size())
    {
      contained.resize(idx + 1);
    }
    bool result = contained[idx];
    contained[idx] = true;
    return result;
  }

  void clear()
  {
    for (Var var : used)
    {
      contained[var] = false;
    }
    used.clear();
  }
};

template <typename T>
class Formula
{
private:
  std::vector<std::unique_ptr<Inequality<T>>> constraints;
  static const unsigned int start_out_id = 0;

public:
  bool hasObjective = false;
  std::unordered_map<int, T> objectiveTerms;
  T objectiveConstant = 0;
  std::unordered_map<std::string, int> alias_to_id;
  std::unordered_map<int, std::string> id_to_alias;

  size_t maxVar;
  size_t claimedNumC;
  size_t claimedNumVar;
  size_t inputFileNumConstraints = 0;

  std::vector<Inequality<T> *> getConstraints()
  {
    std::vector<Inequality<T> *> result;
    result.reserve(constraints.size());

    for (auto &ptr : constraints)
    {
      result.push_back(ptr.get());
    }
    return result;
  }

  void add(std::unique_ptr<Inequality<T>> &&constraint)
  {
    constraints.emplace_back(std::move(constraint));
  }

  int get_next_id()
  {
    return constraints.size() + 1;
  }
};

template <typename CoeffType, typename DegreeType>
struct CoeffNormalizer
{
  CoeffType coeff;
  Lit lit;

  DegreeType normalize()
  {
    if (coeff < 0)
    {
      coeff = -coeff;
      lit = ~lit;
      return coeff;
    }
    else
    {
      return 0;
    }
  }

  template <typename T>
  void emplaceInto(T &vec)
  {
    vec.emplace_back(coeff, lit);
  }
};

template <typename T>
class ParsedTerms
{
public:
  using SmallInt = int32_t;
  static const int smallMaxCharCount = 9;

private:
  CoeffNormalizer<SmallInt, T> small;
  CoeffNormalizer<T, T> large;

  bool isSmall;
  bool hasDuplicates;

  VarDouplicateDetection duplicateDetection;

public:
  bool isClause;
  T degree;
  bool isEq;

  std::vector<Term<T>> largeTerms;
  std::vector<Term<int32_t>> smallTerms;

  ParsedTerms() { reset(); }

  void reset()
  {
    degree = 0;
    largeTerms.clear();
    smallTerms.clear();
    duplicateDetection.clear();
    isEq = false;
    isClause = true;
    isSmall = true;
    hasDuplicates = false;
  }

  void nextSmallCoeff(SmallInt coeff)
  {
    if (this->isSmall)
    {
      this->small.coeff = coeff;
      this->isClause &= (coeff == 1 || coeff == -1);
    }
    else
    {
      this->large.coeff = coeff;
    }
  }

  void switchToLargeTerms()
  {
    this->isClause = false;
    this->isSmall = false;
    if (!smallTerms.empty())
    {
      assert(largeTerms.empty());
      std::copy(smallTerms.begin(), smallTerms.end(),
                std::back_inserter(largeTerms));
      smallTerms.clear();
    }
  }

  void nextLargeCoeff(T &&coeff)
  {
    if (this->isSmall)
    {
      if (coeff <= std::numeric_limits<SmallInt>::max() && coeff > std::numeric_limits<SmallInt>::min())
      {
        this->small.coeff = convertInt<SmallInt>(coeff);
        // We know that the constraint is not a clause but still small.
        this->isClause = false;
      }
      else
      {
        switchToLargeTerms();
        this->large.coeff = std::move(coeff);
      }
    }
    else
    {
      this->large.coeff = std::move(coeff);
    }
  }

  void nextLit(Lit lit)
  {
    if (duplicateDetection.add(lit.var()))
    {
      this->hasDuplicates = true;
      // throw ParseError(it, "Duplicated variables are not supported in
      // constraints.");
    }

    if (this->isSmall)
    {
      small.lit = lit;
      degree += small.normalize();
      small.emplaceInto(smallTerms);
    }
    else
    {
      large.lit = lit;
      degree += large.normalize();
      large.emplaceInto(largeTerms);
    }
  };

  void setDegree(T _degree)
  {
    this->degree += _degree;

    if (this->isSmall)
    {
      if (this->hasDuplicates || this->isEq ||
          this->degree > std::numeric_limits<SmallInt>::max())
      {
        switchToLargeTerms();
      }
      // Check if slack computation can overflow
      if (!this->isClause)
      {
        T slack = -this->degree;
        for (auto const &term : this->smallTerms)
        {
          slack += term.coeff;
        }
        if (slack > std::numeric_limits<SmallInt>::max())
        {
          switchToLargeTerms();
        }
      }
    }

    this->isClause &= this->degree == 1;
  }

  void setEq() { isEq = true; }

  void setGeq() { isEq = false; }

  void flip()
  {
    degree = -degree;
    if (isSmall)
    {
      for (auto &term : smallTerms)
      {
        degree += term.coeff;
        term.lit = ~term.lit;
      }
    }
    else
    {
      for (auto &term : largeTerms)
      {
        degree += term.coeff;
        term.lit = ~term.lit;
      }
    }
  }

  void negate()
  {
    flip();
    degree += 1;
  }

  std::array<std::unique_ptr<Inequality<T>>, 2> getInequalities()
  {
    std::unique_ptr<Inequality<T>> geq;
    if (isClause)
    {
      geq = std::make_unique<Inequality<T>>(
          ClauseHandler(smallTerms.size(), smallTerms.size(),
                        smallTerms.begin(), smallTerms.end()));
    }
    else if (isSmall)
    {
      SmallInt smallDegree = convertInt<SmallInt>(degree);
      if (degree < 0)
      {
        geq = std::make_unique<Inequality<T>>(FixedSizeInequalityHandler<SmallInt>(
            smallTerms.size(), smallTerms, 0));
      }
      else
      {
        geq = std::make_unique<Inequality<T>>(FixedSizeInequalityHandler<SmallInt>(
            smallTerms.size(), smallTerms, smallDegree));
      }
    }
    else
    {
      switchToLargeTerms();
      if (degree < 0)
      {
        geq = std::make_unique<Inequality<T>>(largeTerms, 0);
      }
      else
      {
        geq = std::make_unique<Inequality<T>>(largeTerms, degree);
      }
    }
    std::unique_ptr<Inequality<T>> leq = nullptr;
    if (isEq)
    {
      flip();
      if (degree < 0)
      {
        leq = std::make_unique<Inequality<T>>(largeTerms, 0);
      }
      else
      {
        leq = std::make_unique<Inequality<T>>(largeTerms, degree);
      }
    }

    return {std::move(geq), std::move(leq)};
  }
};

template <typename T>
class OPBParser
{
  VariableNameManager &variableNameManager;
  ParsedTerms<T> parsedTerms;
  VarDouplicateDetection duplicateDetection;

public:
  std::unique_ptr<Formula<T>> formula;

  OPBParser(VariableNameManager &mngr) : variableNameManager(mngr) {}

  std::unique_ptr<Formula<T>> parse(igzstream &f,
                                    const std::string &fileName)
  {
    formula = std::make_unique<Formula<T>>();
    WordIter it(fileName);

    // We currently do not make use of the claimed number of
    // variables (not even for checking if the numbers match),
    // hence let us not parse the header. In general it would be
    // nice to not force the header to be there.
    // if (!WordIter::getline(f, it)) {
    //     throw ParseError(it, "Expected OPB header.");
    // }
    // parseHeader(it);

    bool checkedObjective = false;
    while (WordIter::getline(f, it))
    {
      if (it != WordIter::end && ((*it)[0] != '*'))
      {
        if (!checkedObjective)
        {
          checkedObjective = true;
          if (*it == "min:")
          {
            ++it;
            parseObjective(it);
            continue;
          }
        }

        std::string alias;
        if ((*it)[0] == '@')
        {
          std::string alias{*it};
          formula->alias_to_id.insert({alias, formula->get_next_id()});
          formula->id_to_alias.insert({formula->get_next_id(), alias});
          ++it;
        }

        auto res = parseConstraint(it);
        if (it != WordIter::end)
        {
          throw ParseError(it, "Expected end line after constraint.");
        }
        formula->add(std::move(res[0]));
        if (res[1] != nullptr)
        {
          formula->add(std::move(res[1]));
        }
      }
    }

    return std::move(formula);
  }

  void parseHeader(WordIter &it)
  {
    it.expect("*");
    ++it;
    it.expect("#variable=");
    ++it;
    formula->claimedNumVar = parseInt(it, "Expected number of variables.");
    ++it;
    it.expect("#constraint=");
    ++it;
    formula->claimedNumC = parseInt(it, "Expected number of constraints.");
    ++it;
    if (it != WordIter::end)
    {
      throw ParseError(it, "Expected end of header line.");
    }
  }

  void parseObjective(WordIter &it)
  {
    formula->hasObjective = true;
    while (it != WordIter::end && *it != ";")
    {
      size_t length = it->size();
      if ((*it)[length - 1] == ';')
      {
        length -= 1;
      }
      T coeff = parseCoeff<T>(it, 0, length);
      ++it;
      // If we reached the end of the objective line, then `coeff` is constant term in the objective.
      if (it == WordIter::end || *it == ";")
      {
        formula->objectiveConstant += coeff;
        break;
      }
      Lit lit = this->parseLit(it);

      // Normalize term.
      if (coeff < 0)
      {
        formula->objectiveConstant += coeff;
        coeff = -coeff;
        lit = lit.negated();
      }

      // Normalize objective.
      auto foundTerm = formula->objectiveTerms.find(lit.toInt());
      auto foundNegatedTerm = formula->objectiveTerms.find(lit.negated().toInt());
      if (foundTerm == formula->objectiveTerms.end() && foundNegatedTerm == formula->objectiveTerms.end())
      { // Variable not in the objective yet. Just add term to objective.
        formula->objectiveTerms[lit.toInt()] = coeff;
      }
      else
      { // Literal already in objective
        if (foundTerm != formula->objectiveTerms.end())
        { // Literal has the same sign. Just sum coefficients.
          foundTerm->second += coeff;
        }
        else
        { // Literal has opposite sign. Cancellation possible.
          if (foundNegatedTerm->second > coeff)
          { // Already existing weight is bigger. Subtract new weight and add difference to constant.
            foundNegatedTerm->second -= coeff;
            formula->objectiveConstant += coeff;
          }
          else
          { // New weight is at least existing weight.
            if (foundNegatedTerm->second < coeff)
            { // New weight is bigger. Add new term.
              formula->objectiveTerms[lit.toInt()] = coeff - foundNegatedTerm->second;
            }
            formula->objectiveConstant += foundNegatedTerm->second;
            formula->objectiveTerms.erase(lit.negated().toInt());
          }
        }
      }

      ++it;
    }
  }

  Lit parseLit(WordIter &it)
  {
    Lit lit = ::parseLit(it, variableNameManager);
    if (formula)
    {
      formula->maxVar =
          std::max(formula->maxVar, static_cast<size_t>(lit.var()));
    }
    return lit;
  }

  std::array<std::unique_ptr<Inequality<T>>, 2>
  parseConstraint(WordIter &it, bool geqOnly = false)
  {
    parsedTerms.reset();

    while (it != WordIter::end)
    {
      const string_view &word = *it;
      if (word == ">=" || word == "=" || word == "<=")
      {
        break;
      }

      if (word.size() > parsedTerms.smallMaxCharCount)
      {
        parsedTerms.nextLargeCoeff(parseCoeff<T>(it, 0, it->size()));
      }
      else
      {
        parsedTerms.nextSmallCoeff(
            parseCoeff<typename ParsedTerms<T>::SmallInt>(it, 0, it->size()));
      }
      ++it;

      parsedTerms.nextLit(this->parseLit(it));
      ++it;
    }

    std::vector<std::string> ops = {">=", "=", "<="};
    it.expectOneOf(ops);
    bool isEq = (*it == "=");
    bool isLeq = (*it == "<=");
    if (isEq && geqOnly)
    {
      throw ParseError(it, "Equality not allowed, only >= is allowed here.");
    }

    if (isEq)
      parsedTerms.setEq();
    else
      parsedTerms.setGeq();
    ++it;

    if (it == WordIter::end)
    {
      throw ParseError(it, "Expected degree.");
    }

    bool hasSemicolon = false;
    size_t length = it->size();
    if ((*it)[length - 1] == ';')
    {
      hasSemicolon = true;
      length -= 1;
    }

    parsedTerms.setDegree(parseCoeff<T>(it, 0, length));
    ++it;

    if (!hasSemicolon)
    {
      if (*it == "==>")
      {
        if (parsedTerms.isEq)
        {
          throw ParseError(it, "Can not use implication on equalities.");
        }
        ++it;

        parsedTerms.negate();
        parsedTerms.nextLargeCoeff(T(parsedTerms.degree));
        parsedTerms.nextLit(this->parseLit(it));
        ++it;
      }
      else if (*it == "<==")
      {
        if (parsedTerms.isEq)
        {
          throw ParseError(it, "Can not use implication on equalities.");
        }
        ++it;

        parsedTerms.nextLargeCoeff(T(parsedTerms.degree));
        parsedTerms.nextLit(~this->parseLit(it));
        ++it;
      }

      ++it;
    }

    // Negate parsed terms if we have a leq constraint.
    if (isLeq)
    {
      parsedTerms.flip();
      parsedTerms.isClause = false;
    }

    return parsedTerms.getInequalities();
  }
};

template <typename T>
class CNFParser
{
  VariableNameManager &variableNameManager;
  std::vector<Term<T>> terms;
  VarDouplicateDetection duplicateDetection;

  std::unique_ptr<Formula<T>> formula;
  bool weighted;
  T weighted_partial_top; // Max weight of clause, which generates hard clause
  int vars_claimed_header;
  bool new_format;

public:
  CNFParser(VariableNameManager &mngr, bool weighted = false)
      : variableNameManager(mngr), weighted(weighted) {}

  std::unique_ptr<Formula<T>> parse(igzstream &f,
                                    const std::string &fileName)
  {
    formula = std::make_unique<Formula<T>>();

    if (weighted)
    {
      formula->hasObjective = true;
    }

    WordIter it(fileName);

    bool foundHeader = false;
    while (WordIter::getline(f, it))
    {
      if (it != WordIter::end && ((*it)[0] != 'c'))
      {
        if (!foundHeader)
        {
          parseHeader(it);
          foundHeader = true;
          if (new_format)
          {
            auto res = parseConstraint(it);
            if (res != nullptr)
            {
              formula->add(std::move(res));
            }
          }
        }
        else
        {
          auto res = parseConstraint(it);
          if (res != nullptr)
          {
            formula->add(std::move(res));
          }
        }
      }
    }

    if (!foundHeader)
    {
      if (!weighted)
      {
        throw ParseError(it, "Expected CNF header.");
      }
    }

    return std::move(formula);
  }

  void parseHeader(WordIter &it)
  {
    if (*it != "p")
    {
      formula->claimedNumVar = -1;
      vars_claimed_header = formula->claimedNumVar;
      formula->claimedNumC = -1;
      weighted_partial_top = 0;
      new_format = true;
    }
    else
    {
      it.expect("p");
      ++it;

      if (weighted)
      {
        it.expect("wcnf");
      }
      else
      {
        it.expect("cnf");
      }
      ++it;
      formula->claimedNumVar = parseInt(it, "Expected number of variables.");
      vars_claimed_header = formula->claimedNumVar;
      ++it;
      formula->claimedNumC = parseInt(it, "Expected number of constraints.");
      ++it;

      if (weighted)
      {
        if (it != WordIter::end)
        {
          weighted_partial_top = parseCoeff<T>(it);
        }
        else
        {
          weighted_partial_top =
              std::numeric_limits<T>::max(); // TODO: check for the weights
        }

        ++it;
      }

      new_format = false;

      if (it != WordIter::end)
      {
        throw ParseError(it, "Expected end of header line.");
      }
    }
  }

  Lit parseDIMACSLit(WordIter &it)
  {
    char buffer[12];
    char *bufferEnd = buffer + 12;
    char *bufferStart;

    // parse int to give nice error messages if input is not
    // an integer, out put is not used because we construct
    // string to be consistent with arbitrary variable names
    int parsed_lit = parseInt(it, "Expected literal.");
    if (it->size() > 11)
    {
      throw ParseError(it, "Literal too large.");
    }

    // Check to see that no variables higher than the claimed variables in the
    // header are parsed
    if (abs(parsed_lit) > vars_claimed_header && !new_format)
    {
      throw ParseError(
          it, "More variables in the formula than claimed by the header.");
    }

    bufferStart = bufferEnd - it->size();
    std::copy(it->begin(), it->end(), bufferStart);
    if (bufferStart[0] == '-')
    {
      bufferStart -= 1;
      bufferStart[0] = '~';
      bufferStart[1] = 'x';
    }
    else
    {
      bufferStart -= 1;
      bufferStart[0] = 'x';
    }

    std::string_view litString(bufferStart, bufferEnd - bufferStart);
    return parseLit(it, variableNameManager, &litString);
  }

  std::unique_ptr<Inequality<T>> parseConstraint(WordIter &it)
  {
    formula->inputFileNumConstraints++;
    terms.clear();
    duplicateDetection.clear();

    bool hasDuplicates = false;
    std::unordered_set<Lit> prev_lits;

    T weight = 0;
    bool isSoft = false;
    bool isHard = false;

    while (it != WordIter::end && *it != "0")
    {
      // If no weight has been parsed yet, this means that the first value will
      // be the weight of the clause. If the soft clause is unit, add literal
      // negated to objective function. If soft clause not unit, add a negated
      // relaxation variable with name "bi", where i is the constraint id of the
      // constraint that is currently parsed.

      if (weighted && !isHard && !isSoft)
      {
        if (*it != "h")
        {
          weight = parseCoeff<T>(it);
          if (weight != weighted_partial_top)
          {
            isSoft = true;
          }
          else
          {
            isHard = true;
          }
        }
        else
        {
          isHard = true;
        }

        if (isSoft)
        {
          // Check if soft clause is unit
          int first_lit_start = it.nextStart;
          ++it;
          // Check if soft clause is not empty
          if (*it != "0")
          {
            Lit firstLit = ~parseDIMACSLit(it);
            ++it;
            if (*it == "0")
            { // Is unary clause
              assert(weight >= 1);
              // First, see if the variable already was added by a unit soft clause before.
              auto foundTerm = formula->objectiveTerms.find(firstLit.toInt());
              auto foundNegatedTerm = formula->objectiveTerms.find(firstLit.negated().toInt());

              if (foundTerm == formula->objectiveTerms.end() && foundNegatedTerm == formula->objectiveTerms.end())
              { // Objective variable not known yet.
                formula->objectiveTerms[firstLit.toInt()] = weight;
                formula->maxVar = std::max(formula->maxVar,
                                           static_cast<size_t>(firstLit.var()));
              }
              else
              { // Objective variable known already. Update the coefficient with the coefficient of the current soft unit clause.
                if (foundTerm != formula->objectiveTerms.end())
                { // Term in objective with the same sign. Just add the weights.
                  foundTerm->second += weight;
                }
                else
                { // Term in objective with opposite sign. Cancellation will happen.
                  if (foundNegatedTerm->second > weight)
                  { // Already existing weight is bigger. Subtract new weight and add difference to constant.
                    foundNegatedTerm->second -= weight;
                    formula->objectiveConstant += weight;
                  }
                  else
                  { // New weight is at least existing weight.
                    if (foundNegatedTerm->second < weight)
                    { // New weight is bigger. Add new term.
                      formula->objectiveTerms[firstLit.toInt()] = weight - foundNegatedTerm->second;
                    }
                    formula->objectiveConstant += foundNegatedTerm->second;
                    formula->objectiveTerms.erase(firstLit.negated().toInt());
                  }
                }
              }

              return nullptr;
            }
          }

          // Is not unary clause
          it.nextStart = first_lit_start;

          if (!new_format)
          {
            formula->claimedNumVar++; // Claim another variable as relaxation
                                      // variable.
          }
          std::string name =
              "_b" + std::to_string(formula->inputFileNumConstraints);
          string_view relaxVarName(name);

          Lit negated_lit = ~variableNameManager.getLit(
              relaxVarName); // getLit also calls getVar, which adds the
                             // variable name to the variable name manager
                             // if not exists.

          terms.emplace_back(1, negated_lit);

          formula->objectiveTerms[negated_lit.toInt()] = weight;

          formula->maxVar =
              std::max(formula->maxVar, static_cast<size_t>(negated_lit.var()));
        }
        ++it;
        continue;
      }

      Lit lit = parseDIMACSLit(it);
      if (formula)
      {
        formula->maxVar =
            std::max(formula->maxVar, static_cast<size_t>(lit.var()));
      }

      if (prev_lits.insert(lit).second)
      {
        terms.emplace_back(1, lit);
      }

      if (duplicateDetection.add(lit.var()))
      {
        hasDuplicates = true;
      }

      // Note that duplicate variables will be handled during
      // construction of the Inequality that is if a literal
      // appears twice it will get coefficient 2, if a variable
      // appears with positive as well as negative polarity then
      // a cancellation will be triggered reducing the degree to
      // 0 and thereby trivializing the constraint.

      // todo: maybe add warning if duplicate variables occur?

      ++it;
    }
    if (*it != "0")
    {
      throw ParseError(it, "Expected '0' at end of clause.");
    }
    ++it;

    if (it != WordIter::end)
    {
      throw ParseError(it, "Expected end line after constraint.");
    }

    if (hasDuplicates)
    {
      return std::make_unique<Inequality<T>>(terms, 1);
    }
    else
    {
      return std::make_unique<Inequality<T>>(ClauseHandler(
          terms.size(), terms.size(), terms.begin(), terms.end()));
    }
  }
};

template <typename T>
std::unique_ptr<Formula<T>> parseOpb(std::string fileName,
                                     VariableNameManager &varMgr)
{
  igzstream f(fileName);
  OPBParser<T> parser(varMgr);
  std::unique_ptr<Formula<T>> result = parser.parse(f, fileName);
  return result;
}

template <typename T>
std::array<std::unique_ptr<Inequality<T>>, 2>
parseOpbConstraint(VariableNameManager &varMgr, WordIter &it)
{
  OPBParser<T> parser(varMgr);
  return parser.parseConstraint(it);
}

template <typename T>
std::unique_ptr<Formula<T>>
parseOpbObjective(VariableNameManager &varMgr, WordIter &it)
{
  OPBParser<T> parser(varMgr);
  parser.formula = std::make_unique<Formula<T>>();
  parser.parseObjective(it);
  return std::move(parser.formula);
}

template <typename T>
std::unique_ptr<Formula<T>> parseCnf(std::string fileName,
                                     VariableNameManager &varMgr)
{
  igzstream f(fileName);
  CNFParser<T> parser(varMgr);
  std::unique_ptr<Formula<T>> result = parser.parse(f, fileName);
  return result;
}

template <typename T>
std::unique_ptr<Formula<T>> parseWcnf(std::string fileName,
                                      VariableNameManager &varMgr)
{
  igzstream f(fileName);
  CNFParser<T> parser(varMgr, true);
  std::unique_ptr<Formula<T>> result = parser.parse(f, fileName);
  return result;
}

/// Main function to test parser.
// int main(int argc, char const *argv[])
// {
//   std::chrono::duration<double> timeAttach;
//   std::chrono::duration<double> timeParse;
//   std::chrono::duration<double> timeDetach;

//   std::cout << "start reading file..." << std::endl;
//   std::string proofFileName(argv[2]);
//   std::string instanceFileName(argv[1]);
//   std::ifstream proof(proofFileName);
//   VariableNameManager manager(false);
//   auto formula = parseCnf<CoefType>(instanceFileName, manager);
//   PropEngine<CoefType> engine(formula->maxVar);
//   std::vector<InequalityPtr<CoefType>> constraints;
//   constraints.push_back(nullptr);
//   uint64_t id = 0;
//   for (auto *constraint : formula->getConstraints())
//   {
//     engine.attach(constraint, ++id);
//     constraints.push_back(nullptr);
//   }

//   uint64_t formulaIds = id;

//   size_t rupSteps = 0;
//   size_t nLits = 0;
//   size_t delSteps = 0;
//   size_t count = 0;
//   ProofContext proofContext;
//   WordIter it(argv[1]);
//   while (WordIter::getline(proof, it))
//   {
//     if (it.get() == "pseudo-Boolean")
//     {
//     }
//     else if (it.get() == "f")
//     {
//     }
//     else if (it.get() == "a")
//     {
//       auto c = parseOpbConstraint<CoefType>(manager, it);
//       Inequality<CoefType> *ineq;
//       {
//         Timer timer(timeAttach);
//         ineq = engine.attach(c[0].get(), ++id);
//       }

//       if (ineq == c[0].get())
//       {
//         constraints.emplace_back(std::move(c[0]));
//       }
//       else
//       {
//         constraints.push_back(nullptr);
//       }
//     }
//     else if (it.get() == "u")
//     {
//       rupSteps += 1;
//       it.next();
//       auto c = parseOpbConstraint<CoefType>(manager, it);
//       bool onlyCore = false;
//       if (!c[0]->rupCheck(engine, onlyCore, proofContext))
//       {
//         std::cout << "Verification Failed." << std::endl;
//         return 1;
//       };
//       // nLits += c[0]->size();
//       Inequality<CoefType> *ineq;
//       {
//         Timer timer(timeAttach);
//         ineq = engine.attach(c[0].get(), ++id);
//       }

//       if (ineq == c[0].get())
//       {
//         constraints.emplace_back(std::move(c[0]));
//       }
//       else
//       {
//         constraints.push_back(nullptr);
//       }
//     }
//     else if (it.get() == "del")
//     {
//       delSteps += 1;
//       it.next();
//       it.next();
//       auto c = parseOpbConstraint<CoefType>(manager, it);
//       auto ineq = engine.find(c[0].get());
//       if (ineq == nullptr)
//       {
//         std::cout << "Verification Failed." << std::endl;
//         return 1;
//       }
//       assert(ineq->ids.size() > 0);
//       uint64_t id = 0;
//       for (uint64_t tid : ineq->ids)
//       {
//         id = tid;
//         if (id != ineq->minId)
//         {
//           break;
//         }
//       }
//       {
//         Timer timer(timeDetach);
//         engine.detach(ineq, id);
//       }
//       if (ineq->ids.size() == 0)
//       {
//         assert(id <= formulaIds || constraints[id] != nullptr);
//         constraints[id] = nullptr;
//       }
//     }
//     else if (it.get() == "c")
//     {
//     }
//     else
//     {
//       while (!it.isEnd())
//       {
//         std::cout << it.get() << " ";
//         it.next();
//       }
//       count += 1;
//       std::cout << "\n";
//     }
//   }

//   std::cout << "unkonwn lines:" << count << std::endl;

//   std::cout << "avgSize:" << ((float)nLits) / rupSteps << std::endl;

//   std::cout << "rupSteps: " << rupSteps << std::endl;
//   std::cout << "delSteps: " << delSteps << std::endl;

//   std::cout << "c statistic: time attach: " << std::fixed
//             << std::setprecision(2) << timeAttach.count() << std::endl;

//   std::cout << "c statistic: time detach: " << std::fixed
//             << std::setprecision(2) << timeDetach.count() << std::endl;

//   engine.printStats();
//   return 0;
// }

#ifdef PY_BINDINGS
void init_parsing(py::module &m)
{
  m.doc() = "Efficient implementation for parsing opb and pbp files.";
  m.def("parseOpb", &parseOpb<CoefType>,
        "Parse opb file with fixed precision.");
  m.def("parseCnf", &parseCnf<CoefType>,
        "Parse cnf file with fixed precision.");
  m.def("parseWcnf", &parseWcnf<CoefType>,
        "Parse wcnf file with fixed precision.");

  m.def("parseConstraintOpb", &parseOpbConstraint<CoefType>,
        "Parse opb consraint with fixed precision.");
  m.def("parseObjective", &parseOpbObjective<CoefType>,
        "Parse objective with fixed precision.");

  py::register_exception_translator([](std::exception_ptr p)
                                    {
    try {
      if (p)
        std::rethrow_exception(p);
    } catch (const ParseError &e) {
      py::object pyParseError =
          py::module::import("veripb.exceptions").attr("DirectParseError");
      PyErr_SetString(pyParseError.ptr(), e.what());
    } });

  py::class_<VariableNameManager>(m, "VariableNameManager")
      .def(py::init<bool>())
      .def("maxVar", &VariableNameManager::maxVar)
      .def("getVar",
           [](VariableNameManager &mngr, std::string name)
           {
             return static_cast<uint64_t>(mngr.getVar(name));
           })
      .def("getLit",
           [](VariableNameManager &mngr, std::string name)
           {
             return static_cast<int64_t>(mngr.getLit(name));
           })
      .def("getName", [](VariableNameManager &mngr, LitData value)
           { return mngr.getName(Var(value)); });

  py::class_<igzstream>(m, "ifstream")
      .def(py::init<std::string>())
      .def("close", &igzstream::close);

  m.def("nextLine", &nextLine);

  py::class_<WordIter>(m, "WordIter")
      .def(py::init<std::string>())
      .def("next", &WordIter::operator++)
      .def("undo", &WordIter::undo)
      .def("get", &WordIter::get)
      .def("isEnd", &WordIter::isEnd)
      .def("getFileName", &WordIter::getFileName)
      .def("getLine", &WordIter::getLine)
      .def("getLineText", &WordIter::getLineText)
      .def("setLineText", &WordIter::setLineText)
      .def("getColumn", &WordIter::getColumn);

  py::class_<Formula<CoefType>>(m, "Formula")
      .def(py::init<>())
      .def("getConstraints", &Formula<CoefType>::getConstraints,
           py::return_value_policy::reference_internal)
      .def_readonly("maxVar", &Formula<CoefType>::maxVar)
      .def_readonly("claimedNumC", &Formula<CoefType>::claimedNumC)
      .def_readonly("claimedNumVar", &Formula<CoefType>::claimedNumVar)
      .def_readonly("hasObjective", &Formula<CoefType>::hasObjective)
      .def_readonly("objectiveTerms", &Formula<CoefType>::objectiveTerms)
      .def_readonly("objectiveConstant", &Formula<CoefType>::objectiveConstant)
      .def_readonly("alias_to_id", &Formula<CoefType>::alias_to_id)
      .def_readonly("id_to_alias", &Formula<CoefType>::id_to_alias);
}
#endif
