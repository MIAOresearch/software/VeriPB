from time import perf_counter
import logging

from veripb import InvalidProof
from veripb.rules import Rule, EmptyRule
from veripb.rules_register import register_rule
from veripb.parser import OPBParser, MaybeWordParser


@register_rule
class IsDeleted(EmptyRule):
    Ids = ["is_deleted"]

    @classmethod
    def parse(cls, line, context):
        cppWordIter = line.wordIter.getNative()
        ineq = context.ineqFactory.parse(cppWordIter, allowMultiple=False)
        found = context.propEngine.find(ineq, False)

        return cls(found)

    def __init__(self, found):
        self.found = found

    def compute(self, antecedents, context):
        if self.found:
            raise InvalidProof("Constraint should be deleted.")
        return []


@register_rule
class Fail(EmptyRule):
    Ids = ["fail"]

    @classmethod
    def parse(cls, line, context):
        return cls()

    def compute(self, antecedents, context):
        raise InvalidProof("Proof contains fail instruction.")

@register_rule
class StartTime(EmptyRule):
    Ids = ["start_time"]

    @classmethod
    def parse(cls, line, context):
        name = next(line)

        return cls(name)

    def __init__(self, name):
        self.name = name

    def compute(self, antecedents, context):
        if context.running_timer_custom.get(self.name) is None:
            context.running_timer_custom[self.name] = perf_counter()
        else:
            logging.warning("The timer '{}' has already been started!".format(self.name))

        return []

@register_rule
class EndTime(EmptyRule):
    Ids = ["end_time"]

    @classmethod
    def parse(cls, line, context):
        name = next(line)

        return cls(name)

    def __init__(self, name):
        self.name = name

    def compute(self, antecedents, context):
        end = perf_counter()
        start = context.running_timer_custom.get(self.name)
        if start is None:
            logging.warning("No timer with the name '{}' has already been started, hence the timer can not be ended!".format(self.name))
        else:
            context.total_time_custom[self.name] += end - start
            del context.running_timer_custom[self.name]

        return []
