from collections import defaultdict
import itertools

from veripb.rules import DummyRule, HeaderRule, LoadFormula
from veripb import InvalidProof
from veripb.timed_function import TimedFunction

import sys
import logging
import time
import argparse

from veripb.printing import print_pretty


if hasattr(sys, "set_int_max_str_digits"):
    sys.set_int_max_str_digits(0)


class Context():
    def __init__(self):
        self.proof = None
        self.running_timer_custom = dict()
        self.total_time_custom = defaultdict(float)
        self.aliases = dict()

    def to_constraint_id(self, reference_string):
        if reference_string[0] == "@":
            return self.aliases[reference_string]
        else:
            return int(reference_string)


class DummyPropEngine():
    def attach(self, ineq):
        pass

    def detach(self, ineq):
        pass

    def attachTmp(self):
        raise RuntimeError()

    def isConflicting(self):
        raise RuntimeError()

    def reset(self):
        raise RuntimeError()

# Print iterations progress


def printProgressBar(iteration, total, start_time, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r", stream=sys.stderr):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
        stream      - Optional  : output stream (File object)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 *
                                                     (iteration / float(total)))
    time_left = 0 if iteration == 0 else int(
        round((time.time() - start_time)*(float(total)/iteration-1)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %ds remaining %s' %
          (prefix, bar, percent, time_left, suffix), end=printEnd, file=stream)
    # Print New Line on Complete
    if iteration == total:
        print(file=stream)


class VerificationResult():
    def __init__(self):
        self.isSuccessfull = False
        self.usesAssumptions = False
        self.containsContradiction = False
        self.is_finished = False
        self.has_conclusion = False
        self.has_output = False
        self.requireUnsat = None
        self.major_version = 1

    def print(self):
        if self.major_version == 1 and not self.containsContradiction and self.requireUnsat is None:
            logging.warn("The provided proof did not claim contradiction.")

        if self.usesAssumptions:
            logging.warn("Proof is based on unjustified assumptions.")

        print("Verification succeeded.")


class Verifier():
    """
    Class to veryfi a complete proof.

    Attributese:
        db      the database of proof lines
        goals   index into db for lines that are needed for
                verification
        state   indicates the current state of the solve process
    """

    class Settings():
        def __init__(self, preset=None):
            self.setPreset(type(self).defaults(), unchecked=True)

            if preset is not None:
                self.setPreset(preset)

        def setPreset(self, preset, unchecked=False):
            for key, value in preset.items():
                if unchecked or hasattr(self, key):
                    setattr(self, key, value)
                else:
                    raise ValueError("Got unknown setting %s." % key)

        @staticmethod
        def defaults():
            return {
                "isInvariantsOn": False,
                "trace": False,
                "traceFailed": False,
                "progressBar": False,
                "proofGraph": None,
                "requireUnsat": None,
                "isCheckDeletionOn": True,
                "forceCheckDeletion": False,
                "useColor": False,
                "checkOutputConstraints": False,
                "toAnnotatedRUP": True
            }

        def computeNumUse(self):
            return self.lazy or not self.disableDeletion

        @classmethod
        def addArgParser(cls, parser, name="verifier"):
            defaults = cls.defaults()
            group = parser.add_argument_group(name)

            group.add_argument("--invariants", dest=name+".isInvariantsOn",
                               action="store_true",
                               default=defaults["isInvariantsOn"],
                               help="Turn on invariant checking for debugging (might slow down checking).")
            group.add_argument("--no-invariants", dest=name+".isInvariantsOn",
                               action="store_false",
                               help="Turn off invariant checking.")

            group.add_argument("--checkDeletion", dest=name+".isCheckDeletionOn",
                               action="store_true",
                               default=defaults["isCheckDeletionOn"],
                               help="Turn on checking deletions in core. If the check fails, then checking will switch to unchecked deletion if checked deletion is not forced by '--forceCheckDeletion'.")
            group.add_argument("--no-checkDeletion", dest=name+".isCheckDeletionOn",
                               action="store_false",
                               help="Disable checking deletions in core.")

            group.add_argument("--forceCheckDeletion", dest=name+".forceCheckDeletion",
                               action="store_true",
                               default=defaults["forceCheckDeletion"],
                               help="Turn on forced checking of deletions from core set. If the check fails, then the proof will fail.")
            group.add_argument("--no-forceCheckDeletion", dest=name+".forceCheckDeletion",
                               action="store_false",
                               help="Disable forcing checking deletions from core. If checked deletion is enabled and check fails then proof will switch to unchecked deletion.")

            group.add_argument("--toAnnotatedRUP", dest=name+".toAnnotatedRUP", action="store_true",
                               help="Elaborate RUP to RUP with hints instead of cutting planes.",
                               default=True)
            group.add_argument("--no-toAnnotatedRUP", dest=name+".toAnnotatedRUP", action="store_false",
                               help="Elaborate RUP to cutting planes instead of RUP with hints.",
                               default=True)

            group.add_argument("--requireUnsat", dest=name+".requireUnsat",
                               action="store_true",
                               default=defaults["requireUnsat"],
                               help="Require proof to contain contradiction.")
            group.add_argument("--no-requireUnsat", dest=name+".requireUnsat",
                               action="store_false",
                               help="Do not require proof to contain contradiction, suppress warning.")

            group.add_argument("--useColor", dest=name+".useColor",
                               action="store_true",
                               default=defaults["useColor"],
                               help="Enable colored trace output.")
            group.add_argument("--no-useColor", dest=name+".useColor",
                               action="store_false",
                               help="Disable colored trace output.")

            group.add_argument("--trace", dest=name+".trace",
                               action="store_true",
                               default=False,
                               help="Print the trace of derived constraints.")

            group.add_argument("--traceFailed", dest=name+".traceFailed",
                               action="store_true",
                               default=False,
                               help="Print the trace of propagations and their reasons when a RUP check fails.")

            group.add_argument("--proofGraph", dest=name+".proofGraph",
                               type=argparse.FileType('w'),
                               default=defaults["proofGraph"],
                               help="Write proof graph to given file.")

            group.add_argument("--progressBar", dest=name+".progressBar",
                               action="store_true",
                               default=False,
                               help="Print a progress bar to stderr.")

        @classmethod
        def extract(cls, result, name="verifier"):
            preset = dict()
            defaults = cls.defaults()
            for key in defaults:
                try:
                    preset[key] = getattr(result, name + "." + key)
                except AttributeError:
                    pass
            return cls(preset)

        def __repr__(self):
            return type(self).__name__ + repr(vars(self))

    def print_stats(self):
        print("c statistic: num rules checked: %i" % (self.checked_rules))

    def antecedents(self, ids, ruleNum, ignore_deleted):
        has_both = False
        if type(ids) is tuple:
            has_both = True
            ids = ids[1]
        if ids != "all":
            for i in ids:
                if i >= len(self.db):
                    raise InvalidProof("Rule %i is trying to access constraint "
                                       "(constraintId %i), which is not derived, yet."
                                       % (ruleNum, i))
                elif i <= -len(self.db):
                    raise InvalidProof("Rule %i is trying to access invalid id "
                                       "(constraintId %i)."
                                       % (ruleNum, len(self.db) + i))

                constraint = self.db[i]
                if constraint is None:
                    if ignore_deleted:
                        continue
                    else:
                        raise InvalidProof("Rule %i is trying to access constraint "
                                           "(constraintId %i), that was marked as safe to delete."
                                           % (ruleNum, i))

                if i >= 0:
                    yield (constraint, i)
                else:
                    yield (constraint, len(self.db) + i)
        # Signal the end of the first iterator
        if has_both:
            yield None
        if ids == "all" or has_both:
            for Id, c in enumerate(self.db):
                if c is not None:
                    # this is inconsistent now as we get (Id, c) for
                    # "all" but the constraint directly if a list of
                    # ids is provided. I don' have time to fix this
                    # now and probably nobody (also not future me)
                    # will notice as you ever only want the Ids if you
                    # don't know them already.
                    yield (Id, c)

    def __init__(self, context, settings=None):
        if settings is not None:
            self.settings = settings
        else:
            self.settings = Verifier.Settings()

        if context is None:
            context = Context()
            context.propEngine = DummyPropEngine()

        context.verifierSettings = self.settings
        # version 2 flags
        context.recorded_solution = False
        context.best_objective_value = None
        context.deletion_checked = self.settings.isCheckDeletionOn
        context.force_checked_deletion = self.settings.forceCheckDeletion
        context.forward_comments_to_output_proof = True
        context.is_finished = False
        context.has_conclusion = False
        context.has_output = False
        context.only_core_subproof = False
        context.strengthening_to_core = False

        self.context = context
        self.checked_rules = 0

    @TimedFunction.time("propEngine.attach")
    def attach(self, constraint, constraintId):
        return self.context.propEngine.attach(constraint, constraintId)

    @TimedFunction.time("propEngine.detach")
    def detach(self, constraint, constraintId):
        return self.context.propEngine.detach(constraint, constraintId)

    def handleRule(self, ruleNum, rule):
        self.checked_rules += 1
        if self.settings.progressBar:
            printProgressBar(ruleNum, self.context.ruleCount,
                             self.start_time, length=50)

        antecedents = self.antecedents(
            rule.antecedentIDs(), ruleNum, rule.ignoreDeletedAntecedents())
        antecedents, antecedentsSelfVerify = itertools.tee(antecedents)

        self.context.db_size = len(self.db)
        constraints = rule.compute(antecedents, self.context)

        if ((type(rule) is HeaderRule and self.context.major == 2) or (type(rule) is LoadFormula and self.context.major == 1)) and self.settings.trace and self.context.objective:
            print_pretty(self.settings.useColor, "  ${cid}Objective${reset}: ${obj}min", " ".join(
                [str(coeff) + " " + self.context.ineqFactory.int2lit(lit) for (lit, coeff) in self.context.objective.items()]), str(self.context.objectiveConstant), "${reset}")

        for idx, constraint in enumerate(constraints):
            if constraint is None:
                continue

            constraintId = len(self.db)
            constraint = self.attach(constraint, constraintId)
            # Set up formula constraints correctly
            if rule.isFormula():
                self.context.propEngine.moveToCore(constraint, constraintId)
                constraint.setOutId(constraintId, idx + 1)
            if rule.isAddToCore() or self.context.only_core_subproof:
                self.context.propEngine.moveToCore(constraint, constraintId)

            if self.settings.trace and ruleNum > 0:
                alias = None
                if rule.isFormula():
                    alias = self.context.formula_id_to_alias.get(idx + 1)
                elif idx == 0:
                    alias = rule.alias
                print_pretty(self.settings.useColor, "  ConstraintId ${cid}%(line)03d${reset}${alias}%(alias)s${reset}: ${ineq}%(ineq)s${reset}" % {
                    "line": constraintId,
                    "ineq": self.context.ineqFactory.toString(constraint),
                    "alias": f" ({alias})"  if alias is not None else ""
                })
            if self.settings.proofGraph is not None and ruleNum > 0:
                ids = rule.antecedentIDs()
                if ids == "all":
                    ids = (i for (i, c) in enumerate(
                        self.db) if c is not None and i > 0)
                f = self.settings.proofGraph
                print("%(ineq)s ; %(line)d = %(antecedents)s" % {
                    "line": constraintId,
                    "ineq": self.context.ineqFactory.toString(constraint),
                    "antecedents": " ".join(map(str, ids))
                }, file=f)

            self.db.append(constraint)

            if idx == 0 and rule.alias is not None:
                self.context.aliases[rule.alias] = constraintId

        # Delete rule expects delete to happen after compute!
        deletedConstraints = [
            i for i in rule.deleteConstraints() if self.db[i] is not None]
        if self.settings.trace and len(deletedConstraints) > 0:
            print_pretty(self.settings.useColor, "  ConstraintId  - : deleting ${cid}%(ineq)s${reset}" % {
                "ineq": ", ".join(map(str, deletedConstraints))
            })

        deleted = dict()
        deleted_core_ids = list()
        deleted_derived_ids = list()
        for i in deletedConstraints:
            ineq = self.db[i]
            if ineq is None:
                continue

            self.db[i] = None

            # These checks have to be done before detach, since the IDs are deleted with detach
            if self.context.proof:
                if ineq.isCoreId(i):
                    if ineq.getOutId(i) != 0:
                        deleted_core_ids.append(ineq.getOutId(i))
                if ineq.isDerivedId(i):
                    if ineq.getOutId(i) != 0:
                        deleted_derived_ids.append(ineq.getOutId(i))

            self.detach(ineq, i)

            deleted[id(ineq)] = ineq

        if self.context.proof and ruleNum > 0:
            if constraints:
                # The constraints that have no output proof ID here are not handled yet, they will be justified in the `justify` function
                # The condition `not id(c) in deleted` is a workaround if the same constraint was added and deleted by a rule
                db_size = len(self.db)
                toJustify = [None if c.getOutId(idx) and not id(c) in deleted else (
                    c, idx) for c, idx in zip(self.db[-len(constraints):], range(db_size - len(constraints), db_size))]
            else:
                toJustify = []
            rule.justify(antecedentsSelfVerify, toJustify, self.context)

            # Selflogging to check if constraints added to DB are derived as expected
            if self.settings.checkOutputConstraints:
                for constraint in toJustify:
                    if constraint is not None:
                        # If subproof is started using begin, then do not check output constraint, as subproof follows
                        if hasattr(rule, 'autoProveAll') and rule.autoProveAll:
                            self.context.proof.print("e", self.context.ineqFactory.toString(
                                constraint), ";", constraint.getOutId())

        # Delete output proof constraint IDs and print deleted IDs
        if len(deleted_core_ids) > 0 and not rule.isOutputProofDeletionPrinted():
            self.context.proof.print("delc", " ".join(
                [str(idx) for idx in deleted_core_ids]))
        if len(deleted_derived_ids) > 0 and not rule.isOutputProofDeletionPrinted():
            self.context.proof.print("deld", " ".join(
                [str(idx) for idx in deleted_derived_ids]))

    def __call__(self, rules):
        rules = iter(rules)
        self.context.rules = rules

        self.db = list()
        # Insert None to database to get indexing correct
        self.db.append(None)
        self.result = VerificationResult()
        self.result.requireUnsat = self.settings.requireUnsat

        if self.settings.trace:
            print()
            print("=== begin trace ===")

        if self.settings.progressBar:
            self.start_time = time.time()

        for ruleNum, rule in enumerate(itertools.chain([DummyRule()], rules)):
            try:
                self.handleRule(ruleNum, rule)
            except InvalidProof as e:
                e.lineInFile = rule.lineInFile
                raise e

        self.result.major_version = self.context.major

        self.result.is_finished = getattr(
            self.context, "is_finished", False)
        self.result.has_conclusion = getattr(
            self.context, "has_conclusion", False)
        self.result.has_output = getattr(
            self.context, "has_output", False)
        self.result.usesAssumptions = getattr(
            self.context, "usesAssumptions", False)
        self.result.containsContradiction = getattr(
            self.context, "containsContradiction", False)

        if self.settings.requireUnsat and not self.result.containsContradiction:
            raise InvalidProof("Proof does not contain contradiction!")

        if self.context.major == 2:
            if not self.result.has_output:
                raise InvalidProof(
                    "Proof is missing output (If proof does not have an output, use 'output NONE').")

            if not self.result.has_conclusion:
                raise InvalidProof(
                    "Proof is missing conclusion (If proof does not have a conclusion, use 'conclusion NONE').")

            if not self.result.is_finished:
                raise InvalidProof(
                    "Proof is missing end (The proof should end with 'end pseudo-Boolean proof').")

        if self.settings.trace:
            print("=== end trace ===")
            print()

        return self.result
