# Introduction
This directory contains the files necessary to run verified proof checking statically on the StarExec cluster.

`veripb/veripb_bin_static` is a statically compiled version of VeriPB, built using `make dist` (on a Linux machine) and tested on the StarExec VM.

`veripb/cake_pb_cnf` contains the files for the verified proof checker CakePB.

# Compiling and Running
To compile the tools, run

`starexec_build`

After compilation, enter the `bin` directory and run

`starexec_run_default foo.cnf`

in order to check an UNSAT proof for `foo.cnf`

The proof checking script expects a proof file `proof.out` in the `bin` directory.

# Important Note for StarExec
The verified proof checker manages its own memory and will be allocated a statically fixed amount of memory on program startup.

In this directory, we have set the static memory allocated to `cake_pb_cnf` to 4GB for its heap and stack so that users can easily test it on their machines.

However, when compiling the tool for use on StarExec, this limit should be increased.

This can be done by modifying the following lines in `veripb/cake_pb_cnf/basis_ffi.c`.
For example, we suggest increasing the default heap and stack sizes as follows:

`unsigned long cml_heap_sz = 65536 * sz; // Default: 64 GB heap`

`unsigned long cml_stack_sz = 16384 * sz; // Default: 16 GB stack`
