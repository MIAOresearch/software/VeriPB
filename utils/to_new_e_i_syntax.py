#!/usr/bin/env python3
import sys
import os

# Script to translate pseudo-Boolean proof files for UNSAT decision instances from version 1.x to version 2.0

if len(sys.argv) < 2:
    print("Usage: python 1.x_to_2.0.py <proof_file_name> [--replace]\n")
    print("--replace    replace original file")
else:
    specific_commands = ["e", "ea", "i", "ia"]
    with open(sys.argv[1], "r") as input_file:
        with open("temp.pbp", "w") as output_file:
            for orig_line in input_file.readlines():
                line = orig_line.strip()
                words = line.split(" ")
                # Directly copy everything except e, i, ea, and ia
                if len(words) == 0:
                    output_file.write(orig_line)
                else:
                    if words[0] not in specific_commands:
                        output_file.write(orig_line)
                    else:
                        if len(words) >= 3 and words[2] == ":":
                            output_file.write(
                                words[0] + " " + " ".join([word for word in words[3:]]) + " " + words[1] + "\n")
                        else:
                            output_file.write(orig_line)

    # Replace old proof file
    if len(sys.argv) >= 3 and sys.argv[2] == "--replace":
        os.rename("temp.pbp", sys.argv[1])
