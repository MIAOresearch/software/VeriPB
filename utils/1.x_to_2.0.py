#!/usr/bin/env python3
import sys
import os

# Script to translate pseudo-Boolean proof files for UNSAT decision instances from version 1.x to version 2.0

if len(sys.argv) < 2:
    print("Usage: python 1.x_to_2.0.py <proof_file_name> [--replace]\n")
    print("--replace    replace original file")
else:
    temp_contradiction = None
    with open(sys.argv[1], "r") as input_file:
        with open("temp.pbp", "w") as output_file:
            for line in input_file.readlines():
                line = line.strip()
                # Directly copy comments
                words = line.split(" ")
                if len(words) >= 1 and words[0] == "*":
                    output_file.write(line + "\n")
                    continue
                # Move contradiction ID from `c` to `end`
                if line == "end" or line == "qed":
                    if temp_contradiction:
                        output_file.write(
                            "end " + temp_contradiction.split(" ")[1] + "\n")
                    else:
                        output_file.write("end\n")
                    temp_contradiction = None
                    continue
                # Contradiction was just used to check if a constraint is a contradiction and not to end a (sub-)proof
                if temp_contradiction:
                    output_file.write(temp_contradiction + "\n")
                    temp_contradiction = None
                    continue
                # If the header already says version 2.0, then don't do translation
                if line == "pseudo-Boolean proof version 2.0":
                    os.remove("temp.pbp")
                    raise SyntaxError("Proof already version 2.0")
                # Change header line
                if line == "pseudo-Boolean proof version 1.0" or line == "pseudo-Boolean proof version 1.1" or line == "pseudo-Boolean proof version 1.2":
                    output_file.write("pseudo-Boolean proof version 2.0\n")
                    continue
                # Copy rule to version 2
                else:
                    if len(words) >= 1:
                        # Temporarily store contradictions
                        if words[0] == "c":
                            temp_contradiction = line
                            continue
                    # Rule is not `c`
                    output_file.write(line + "\n")

            # Add proof footer
            output_file.write("output NONE\n")
            if temp_contradiction:
                words = temp_contradiction.split(" ")
                output_file.write("conclusion UNSAT : " + words[1] + "\n")
            else:
                output_file.write("conclusion NONE\n")
            output_file.write("end pseudo-Boolean proof\n")

    # Replace old proof file
    if len(sys.argv) >= 3 and sys.argv[2] == "--replace":
        os.rename("temp.pbp", sys.argv[1])
    # Rename new file by extending it with "_version2"
    else:
        old_proof_file = os.path.splitext(sys.argv[1])
        new_name = old_proof_file[0] + "_version2" + old_proof_file[1]
        os.rename("temp.pbp", new_name)
